const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const body = req.body
    if ("id" in body) {
        return res.status(400).send({
            error: true,
            message: `Id param is not allowed in request body`
        });
    } else if (checkCreateParams(body)) {
        return res.status(400).send({
            error: true,
            message: `Excess or missing params`
        });
    } else if (checkPower(body)) {
        return res.status(400).send({
            error: true,
            message: `Power is not valid`
        });
    } else if (checkDefense(body)) {
        return res.status(400).send({
            error: true,
            message: `Defense is not valid`
        });
    }
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const body = req.body
    if ("id" in body) {
        return res.status(400).send({
            error: true,
            message: `Id param is not allowed in request body`
        });
    } else if (checkUpdateParams(body)) {
        return res.status(400).send({
            error: true,
            message: `Excess or missing params`
        });
    } else if (checkPower(body)) {
        return res.status(400).send({
            error: true,
            message: `Power is not valid`
        });
    } else if (checkDefense(body)) {
        return res.status(400).send({
            error: true,
            message: `Defense is not valid`
        });
    }
    next();
}

const checkUpdateParams = (body) => {
    Object.keys(body).forEach(element => {
        if (!(element in fighter)) {
            return true
        }
    });
}

const checkPower = (body) => {
    const power = body.power
    if (isNaN(power) || power > 100 || power <= 0) {
        return true
    }
}

const checkDefense = (body) => {
    const defense = body.defense
    if (isNaN(defense) || defense > 10 || defense < 1) {
        return true
    }
}

const checkCreateParams = (body) => {
    const params = Object.keys(fighter)
    const bodyParams = Object.keys(body)
    params.shift()
    return !(JSON.stringify(bodyParams.sort()) === JSON.stringify(params.sort()))
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;