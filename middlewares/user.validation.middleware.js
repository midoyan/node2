const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const body = req.body
    if ("id" in body) {
        return res.status(400).send({
            error: true,
            message: `Id param is not allowed in request body`
        });
    } else if (checkCreateParams(body)) {
        return res.status(400).send({
            error: true,
            message: `Excess or missing params`
        });
    } else if (checkGmail(body)) {
        return res.status(400).send({
            error: true,
            message: `Only valid Gmail emails are allowed`
        });
    } else if (checkPhone(body)) {
        return res.status(400).send({
            error: true,
            message: `Only valid Ukrainian phone numbers are allowed`
        });
    }
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const body = req.body
    if ("id" in body) {
        return res.status(400).send({
            error: true,
            message: `Id param is not allowed in request body`
        });
    } else if (checkUpdateParams(body)) {
        return res.status(400).send({
            error: true,
            message: `Excess params`
        });
    } else if ("email" in body && checkGmail(body)) {
        return res.status(400).send({
            error: true,
            message: `Only valid Gmail emails are allowed`
        });
    } else if ("phoneNumber" in body && checkPhone(body)) {
        return res.status(400).send({
            error: true,
            message: `Only valid Ukrainian phone numbers are allowed`
        });
    }
    next();
}

const checkUpdateParams = (body) => {
    Object.keys(body).forEach(element => {
        if (!(element in user)) {
            return true
        }
    });
}

const checkCreateParams = (body) => {
    const params = Object.keys(user)
    const bodyParams = Object.keys(body)
    params.shift()
    return !(JSON.stringify(bodyParams.sort()) === JSON.stringify(params.sort()))
}

const checkGmail = (body) => {
    const email = body.email
    const regex = /^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/i
    return !(regex.test(email))
}
const checkPhone = (body) => {
    const number = body.phoneNumber
    const regex = /^\+3?8?(0\d{9})$/
    return !(regex.test(number))
}


exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;