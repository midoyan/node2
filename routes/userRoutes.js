const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', (req, res, next) => {
    res.send(UserService.searchAll())
    console.log('get /')
    next()
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    const id = req.params.id
    console.log('get /:id')
    if (!(UserService.search({id}))) {
        res.status(404).send({
            error: true,
            message: 'User not found'
        })
    } else {
        res.send(UserService.search({ id }))
    }
    next()
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    const id = req.params.id
    const data = req.body
    console.log('put /:id', req.body)
    if (!(UserService.search({id}))) {
        res.status(404).send({
            error: true,
            message: 'User not found'
        })
    } else {
        res.send(UserService.update(id, data))
    }
    next()
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
    const user = req.body
    console.log(req.body)
    if (UserService.search({email: user.email}) || UserService.search({phoneNumber: user.phoneNumber})) {
        res.status(400).send({
            error: true,
            message: 'User with such email or phoneNumber already exists'
        })
    } else {
        res.send(UserService.save(user))
    }
    next()
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    const id = req.params.id
    console.log('delete /:id')
    if (!(UserService.search({id}))) {
        res.status(404).send({
            error: true,
            message: 'User not found'
        })
    } else {
        res.send(UserService.delete(id))
    }
    next()
}, responseMiddleware);

module.exports = router;

/*
[
    {
        "firstName": "sdfsdf",
        "lastName": "Dfsdf",
        "email": "ersdf@gmail.com",
        "phoneNumber": "+132423",
        "password": "123788",
        "id": "7ff98f41-9b82-4f24-8844-8801c89f6209",
        "createdAt": "2020-05-16T17:36:34.145Z",
        "updatedAt": "2020-05-16T18:21:37.564Z"
    }
]
*/