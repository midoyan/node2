const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', (req, res, next) => {
    res.send(FighterService.searchAll())
    console.log('get /')
    next()
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    const id = req.params.id
    console.log('get /:id')
    if (!(FighterService.search({id}))) {
        res.status(404).send({
            error: true,
            message: 'Fighter not found'
        })
    } else {
        res.send(FighterService.search({ id }))
    }
    next()
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    const id = req.params.id
    const data = req.body
    console.log('put /:id', req.body)
    if (!(FighterService.search({id}))) {
        res.status(404).send({
            error: true,
            message: 'Fighter not found'
        })
    } else {
        res.send(FighterService.update(id, data))
    }
    next()
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
    const fighter = req.body
    console.log(req.body)
    if (FighterService.search({name: fighter.name})) {
        res.status(400).send({
            error: true,
            message: 'Fighter with such name already exists'
        })
    } else {
        res.send(FighterService.save(fighter))
    }
    next()
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    const id = req.params.id
    console.log('delete /:id')
    if (!(FighterService.search({id}))) {
        res.status(404).send({
            error: true,
            message: 'Fighter not found'
        })
    } else {
        res.send(FighterService.delete(id))
    }
    next()
}, responseMiddleware);

module.exports = router;