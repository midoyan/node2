const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    save(user) {
        const item = FighterRepository.create(user);
        if(!item) {
            return null;
        }
        return item;
    }

    searchAll() {
        const item = FighterRepository.getAll();
        if(!item) {
            return null;
        }
        return item;
    }

    update(id, data) {
        const item = FighterRepository.update(id, data);
        if(!item) {
            return null;
        }
        return item;
    }

    delete(id) {
        const item = FighterRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();