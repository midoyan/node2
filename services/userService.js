const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    save(user) {
        const item = UserRepository.create(user);
        if(!item) {
            return null;
        }
        return item;
    }

    searchAll() {
        const item = UserRepository.getAll();
        if(!item) {
            return null;
        }
        return item;
    }

    update(id, data) {
        const item = UserRepository.update(id, data);
        if(!item) {
            return null;
        }
        return item;
    }

    delete(id) {
        const item = UserRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();